# README #

### Description ###
This service handles the end to end process for a user to book an appointment from a mechanic.

### Usage ###
*  `npm install` - Install all dependencies
*  `npm run build` - Compile TS files to JS
*  `npm run start:devts` - run the _development_ service with ts-node (no build required)
*  `npm run start:devjs` - run the _development_ service with node (build required)
*  `npm start` - to run the built _production_ service
    *  service runs on port 4001
    *  _/playground_ to use graphiql
    *  _/graphql_ to use service endpoint
*  `npm run test` - executes unit tests
    *  Ensure arp-service credentials are set locally
    *  set environment variables _AWS_REGION_ and _AWS_PROFILE_
*  `npm run coverage` - executes code coverage tests
