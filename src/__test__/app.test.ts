import 'mocha';
import { assert } from 'chai';
import init from '../app';

const testApp = init({ logger: { level: 'silent' } });

describe('ec2Test Service', function () {

  describe('GraphQL Server related tests', function () {
    describe('Passing valid graphQL query', function () {
      it('responds with success and data', async function () {
        const query = `{
          ec2Test
        }`;
        const expectedPL = {
          data: {
            ec2Test: 'hello from ec2Test'
          }
        };

        const testResult = await testApp.inject({
          method: 'POST',
          url: '/graphql',
          headers: {
            'Content-Type': 'application/json',
          },
          payload: JSON.stringify({
            query: query,
          })
        });

        assert.equal(testResult.statusCode, 200);
        assert.typeOf(testResult.payload, 'string');
        assert.equal(testResult.payload, JSON.stringify(expectedPL));
      });
    });
  });
});
