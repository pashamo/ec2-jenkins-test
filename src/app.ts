import fastify from 'fastify';
import mercurius from 'mercurius';
import fastifyCors from 'fastify-cors';

//This is the typeDefs for the fastify app wagwan bro
const typeDefs = `
  type Query {
    ec2Test: String!
  }
`;

//This is the resolver for the fastify app wagwan bro
const resolvers = {
  Query: {
    ec2Test: () => "hello from ec2Test"
  },
};

//This is the fastify app Wagwan Bro
const init = (opts = { logger: { level: 'error' } }) => {
  const app = fastify(opts);
  app.register(fastifyCors);
  app.register(mercurius, {
    graphiql: "playground",
    schema: typeDefs,
    resolvers,
  });
  return app;
};

export default init;
