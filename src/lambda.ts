import init from './app';
//Hello
const awsLambdaFastify = require('aws-lambda-fastify');
const handler = awsLambdaFastify(init());
let playgroundHandler;

// playground function for serverless offline
if (process.env.NODE_ENV === 'local') {
  const lambdaPlayground = require('graphql-playground-middleware-lambda')
    .default;
  playgroundHandler = lambdaPlayground({
    endpoint: '/local/graphql',
    settings: { 'schema.polling.enable': false },
  });
}

export { handler, playgroundHandler };
